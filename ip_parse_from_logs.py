import re
import sys

def parse_ip_addresses(file_path):
    ip_addresses = []
    with open(file_path, 'r') as file:
        for line in file:
            match = re.search(r'\b(?:[0-9]{1,3}\.){3}[0-9]{1,3}\b', line)
            if match:
                ip_addresses.append(match.group())
    return ip_addresses

def write_ip_addresses_to_file(ip_addresses, output_file_path):
    with open(output_file_path, 'a') as file:
        for ip_address in ip_addresses:
            file.write(ip_address + '\n')

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("Usage: python script.py input_file_path output_file_path")
        sys.exit(1)

    input_file_path = sys.argv[1]
    output_file_path = sys.argv[2]

    ip_addresses = parse_ip_addresses(input_file_path)
    write_ip_addresses_to_file(ip_addresses, output_file_path)
