
# 1) зачадач
def main():
    # Запрос названия у пользователя
    name = input("Введите название: ")
    # Вывод названия в консоль
    print("Введенное название:", name)
main()

# 2) задача
import sys

def check_case(text):
    '''
    Функция проверяет регистр введенной строки и выводит информацию о ней в консоль.
    '''
    upper_case = 0
    lower_case = 0
    for char in text:
        if char.isupper():
            upper_case += 1
        elif char.islower():
            lower_case += 1

    print(f"Строка: '{text}'")
    print(f"Количество заглавных букв: {upper_case}")
    print(f"Количество строчных букв: {lower_case}")

if __name__ == "__main__":
    if len(sys.argv) > 1:
        check_case(sys.argv[1])
    else:
        print("Пожалуйста, дайте строку в качестве аргумента.")

# 3 задача
word = input("Введите слово: ")
uppercase_letters = [letter.upper() for letter in word]
print (uppercase_letters)


# 4) задача
def main():
    # Запрос названия у пользователя
    n = 0
    while True:
    # Вывод названия в консоль
        n +=1
        if n % 2 == 0:
            yield str(n),("Четное")
        else:
            yield str(n),("Нечетное")
counter = main()

while True:
    print (next (counter))

