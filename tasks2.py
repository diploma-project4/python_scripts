

# 1 задача, проверяет среду исполнения

import sys
def main():
    if get_run_from_command_line():
        print("Запущен из командной строки")
    else:
        print("Запущен не из командной строки")

def get_run_from_command_line():
    if getattr(sys, 'frozen', False):
        return False
    if hasattr(sys, 'argv'):
        return True
    return False

if __name__ == '__main__':
    main()

# 2 задача, проверяет с чего начивается слово используя click

import click
@click.command()
@click.argument('word')
def main(word):
    if not word[0].upper() == 'Y':
        click.echo(word)

if __name__ == '__main__':
    main()


