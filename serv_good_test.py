
import pytest
from serv_good_func import check_server_availability


def test_servers():
    with open("ip_add.txt", "r") as file:
        ip_addresses = file.readlines()
    
    for ip_address in ip_addresses:
        ip_address = ip_address.strip()  # Remove leading/trailing whitespace and newline characters
        assert check_server_availability(ip_address), f"Сервер {ip_address} недоступен или ответил с ошибкой"
