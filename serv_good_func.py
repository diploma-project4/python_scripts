
import requests

def check_server_availability(ip_address):
    try:
        response = requests.get(f"http://{ip_address}:80", timeout=5)
        if response.status_code == 200:
            return True
        else:
            return False
    except requests.RequestException:
        return False
